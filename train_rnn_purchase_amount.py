import tensorflow as tf
import numpy as np
import argparse
from sklearn.externals import joblib as pickle
import random
import scipy.stats 

parser = argparse.ArgumentParser()
parser.add_argument('--numUnits', default = 2)
parser.add_argument('--learning_rate', default = 0.0001)
parser.add_argument('--epochs', default = 100)
numUnits = parser.parse_args().numUnits
learning_rate = parser.parse_args().learning_rate
epochs = parser.parse_args().epochs



class model_purchase_amount(object):
	def __init__(self, numOutput = 2):
		self.load_data()
		self.numOutput = 2

	def rnn_cell(self):
		return tf.contrib.rnn.BasicRNNCell(numUnits)

	def get_data(self,x,y):
		x = x.reshape(len(x[0]), self.windowLen, self.numFeatures)
		y = y[-1]
		return x,y.transpose()

	def load_data(self):
		self.trainX, self.testX, self.trainY, self.testY = pickle.load(open("data.pkl","r"))
		self.nSample = len(self.trainX[0])
		self.windowLen = self.trainX.shape[0]
		self.numFeatures = self.trainX.shape[2]
		self.trainX, self.trainY = self.get_data(self.trainX, self.trainY)
		self.testX, self.testY = self.get_data(self.testX, self.testY)

	def shuffle_(self):
	    permutation = list(range(len(self.trainX)))
	    random.shuffle(permutation)
	    return self.trainX[permutation], self.trainY[permutation]

	def RNN(self, x, weights, biases):

	    # Design a RNN Cell.
	    # Current data input shape: (window_length, num_samples, num_features)
	    # Required shape: 'timesteps' tensors list of shape (batch_size, n_input)

	    # Unstack to get a list of 'timesteps' tensors of shape (batch_size, n_input)
	    x = tf.unstack(x, self.windowLen, 1)

	    # Define a lstm cell with tensorflow
	    lstm_cell = tf.contrib.rnn.BasicLSTMCell(self.numFeatures, forget_bias=1.0, activation = tf.nn.tanh)

	    # Get lstm cell output
	    outputs, state = tf.contrib.rnn.static_rnn(lstm_cell, x, dtype=tf.float32)
	    # Linear activation, using rnn inner loop last output
	    return tf.matmul(outputs[-1], weights['out']) + biases['out']

	def weights(self):
		weights = {
	    'out': tf.Variable(tf.random_normal([self.numFeatures, self.numOutput]))
		}
		return weights

	def biases(self):
		biases = {
	    'out': tf.Variable(tf.random_normal([self.numOutput]))
		}
		return biases

	def train_model(self):
		batch_size = len(self.trainX) // 100
		#tensor_pdf = lambda x : 1/(2*3.14) *tf.exp((-x**2)/2) 
		tensor_cdf = lambda x_ : 1/(1+tf.exp(-x_))
		tensor_pdf = lambda x_ : tf.exp(-x_)/(1+tf.exp(-x_))**2
		x = tf.placeholder(tf.float32,[None, self.windowLen, self.numFeatures])
		y = tf.placeholder(tf.float32,[None]) 
		y_hat = tf.nn.sigmoid(self.RNN(x, self.weights(), self.biases()))
		w0 = y_hat[:,0]
		w1 = y_hat[:,1]
		eps =1e-10
		likelihood = eps + (1-eps) * tensor_pdf(w0 * y  - w1) * w0
		loss = tf.log(likelihood)
		mean_loss = -tf.reduce_mean(loss)
		global_step = tf.Variable(0, name='global_step', trainable=False)
		learning_rate = tf.train.exponential_decay(0.1, global_step, 100, 0.95, staircase=True)
		optimizer = tf.train.AdamOptimizer(learning_rate = learning_rate)
		train_op = optimizer.minimize(mean_loss, global_step=global_step)
		feed_dict = lambda x_,y_ : {x:x_, y:y_}
		feed_dict_test = feed_dict(self.testX, self.testY)
		best_test_loss = 1e+10 
		sess = tf.Session()
		init = tf.global_variables_initializer()
		sess.run(init)
		for epoch in range(epochs) :
			epochTrainX_, epochTrainY_ = self.shuffle_()
			train_losses = []
			for i in range(len(self.trainX)//batch_size):
				start = i*batch_size
				end = (i+1) * batch_size
				epochTrainX = epochTrainX_[start:end]; epochTrainY = epochTrainY_[start:end]
				feed_dict_train = feed_dict(epochTrainX, epochTrainY)
				train_loss,_,loss_, y_hat_,y_, __ =  sess.run([mean_loss, train_op, loss, y_hat,y, learning_rate], feed_dict = feed_dict_train)
				if np.isnan(train_loss):
					import pdb;pdb.set_trace()
				train_losses.append(train_loss)
			test_loss = sess.run(mean_loss, feed_dict = feed_dict_test)
			best_test_loss = test_loss if test_loss <= best_test_loss else best_test_loss
			print "Train_loss: {0} \t Test Loss: {1} Best Test Loss: {2}".format(np.mean(train_losses), test_loss, best_test_loss)

if __name__ == '__main__':
	mPA = model_purchase_amount()
	mPA.train_model()





