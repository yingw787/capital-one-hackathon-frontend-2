
//
// //
// // // For notifications
// //
//
var defaultWidth = window.screen.width > 768 ? window.screen.width*1/3: window.screen.width;

var style = {
    Wrapper: {},
    Containers: {
        DefaultStyle: {
            position: 'fixed',
            width: defaultWidth,
            padding: '10px 10px 10px 20px',
            zIndex: 9998,
            WebkitBoxSizing: '',
            MozBoxSizing: '',
            boxSizing: '',
            height: 'auto',
            display: 'inline-block',
            border: '0',
            fontSize: '14px',
            WebkitFontSmoothing: "antialiased",
            fontFamily: '"Roboto","Helvetica Neue",Arial,sans-serif',
            fontWeight: '400',
            color: '#FFFFFF'

        },

        tl: {
            top: '0px',
            bottom: 'auto',
            left: '0px',
            right: 'auto'
        },

        tr: {
            top: '0px',
            bottom: 'auto',
            left: 'auto',
            right: '0px'
        },

        tc: {
            top: '0px',
            bottom: 'auto',
            margin: '0 auto',
            left: '50%',
            marginLeft: -(defaultWidth / 2)
        },

        bl: {
            top: 'auto',
            bottom: '0px',
            left: '0px',
            right: 'auto'
        },

        br: {
            top: 'auto',
            bottom: '0px',
            left: 'auto',
            right: '0px'
        },

        bc: {
            top: 'auto',
            bottom: '0px',
            margin: '0 auto',
            left: '50%',
            marginLeft: -(defaultWidth / 2)
        }

    },

    NotificationItem: {
        DefaultStyle: {
            position: 'relative',
            width: '100%',
            cursor: 'pointer',
            borderRadius: '4px',
            fontSize: '14px',
            margin: '10px 0 0',
            padding: '10px',
            display: 'block',
            WebkitBoxSizing: 'border-box',
            MozBoxSizing: 'border-box',
            boxSizing: 'border-box',
            opacity: 0,
            transition: 'all 0.5s ease-in-out',
            WebkitTransform: 'translate3d(0, 0, 0)',
            transform: 'translate3d(0, 0, 0)',
            willChange: 'transform, opacity',

            isHidden: {
                opacity: 0
            },

            isVisible: {
                opacity: 1
            }
        },

        success: {
            borderTop: 0,
            backgroundColor: '#a1e82c',
            WebkitBoxShadow: 0,
            MozBoxShadow: 0,
            boxShadow: 0
        },

        error: {
            borderTop: 0,
            backgroundColor: '#fc727a',
            WebkitBoxShadow: 0,
            MozBoxShadow: 0,
            boxShadow: 0
        },

        warning: {
            borderTop: 0,
            backgroundColor: '#ffbc67',
            WebkitBoxShadow: 0,
            MozBoxShadow: 0,
            boxShadow: 0
        },

        info: {
            borderTop: 0,
            backgroundColor: '#63d8f1',
            WebkitBoxShadow: 0,
            MozBoxShadow: 0,
            boxShadow: 0
        }
    },

    Title: {
        DefaultStyle: {
            fontSize: '30px',
            margin: '0',
            padding: 0,
            fontWeight: 'bold',
            color: '#FFFFFF',
            display: 'block',
            left: '15px',
            position: 'absolute',
            top: '50%',
            marginTop: '-15px'
        }

    },

    MessageWrapper: {
        DefaultStyle: {
            marginLeft: '55px',
            marginRight: '30px',
            padding: '0 12px 0 0',
            color: '#FFFFFF',
            maxWidthwidth: '89%'
        }
    },

    Dismiss: {
        DefaultStyle: {
            fontFamily: 'inherit',
            fontSize: '21px',
            color: '#000',
            float: 'right',
            position: 'absolute',
            right: '10px',
            top: '50%',
            marginTop: '-13px',
            backgroundColor: '#FFFFFF',
            display: 'block',
            borderRadius: '50%',
            opacity: '.4',
            lineHeight: '11px',
            width: '25px',
            height: '25px',
            outline: '0 !important',
            textAlign: 'center',
            padding: '6px 3px 3px 3px',
            fontWeight: '300',
            marginLeft: '65px'
        },

        success: {
            // color: '#f0f5ea',
            // backgroundColor: '#a1e82c'
        },

        error: {
            // color: '#f4e9e9',
            // backgroundColor: '#fc727a'
        },

        warning: {
            // color: '#f9f6f0',
            // backgroundColor: '#ffbc67'
        },

        info: {
            // color: '#e8f0f4',
            // backgroundColor: '#63d8f1'
        }
    },

    Action: {
        DefaultStyle: {
            background: '#ffffff',
            borderRadius: '2px',
            padding: '6px 20px',
            fontWeight: 'bold',
            margin: '10px 0 0 0',
            border: 0
        },

        success: {
            backgroundColor: '#a1e82c',
            color: '#ffffff'
        },

        error: {
            backgroundColor: '#fc727a',
            color: '#ffffff'
        },

        warning: {
            backgroundColor: '#ffbc67',
            color: '#ffffff'
        },

        info: {
            backgroundColor: '#63d8f1',
            color: '#ffffff'
        }
    },

    ActionWrapper: {
        DefaultStyle: {
            margin: 0,
            padding: 0
        }
    }
}

//
// //
// // // For tables
// //
//

const thArrayPurchaseAmountPersonal = ["Merchant Name","Purchase Amount","Month","Day","Year","Zipcode"];
const tdArrayPurchaseAmountPersonal = [
    ["amazon video on demand", "122.17", "October", "23", "2017", "11205"],
    ["onstar subscription", "116.56", "October", "23", "2017", "66286"],
    ["altus dental insur", "55.42", "October", "22", "2017", "73129"],
    ["cartoon carwash", "140.01", "October", "22", "2017", "80150"],
    ["usa*premier services vend", "377.91", "October", "21", "2017", "22908"]
];

const tdArrayPurchaseAmountAllen = [
    ["abc*retro fitness", "162.6", "October", "18", "2017", "28235"],
    ["speedline solutions in", "166.67", "October", "17", "2017", "88546"],
    ["credit monitor 8663116", "151.3", "October", "15", "2017", "16534"],
    ["gillette stadium", "80.52", "October", "12", "2017", "50320"],
    ["hgtv magazine", "306.79", "October", "11", "2017", "20220"]
];

const tdArrayPurchaseAmountJohn= [
    ["gillette stadium", "80.52", "October", "12", "2017", "50320"],
    ["hgtv magazine", "100.79", "October", "11", "2017", "20220"],
    ["hamilton co clerk", "122.22", "October", "11", "2017", "17110"],
    ["parkmobile", "179.98", "October", "11", "2017", "78285"],
    ["yard bright lighting", "399.79", "October", "10", "2017", "22309"],
    ["state farm insurance", "15.71", "October", "10", "2017", "45228"]
];

const tdArrayPurchaseAmountLacy = [
    ["fanball", "55.87", "October", "6", "2017", "98447"],
    ["tractor supply co #704", "233.97", "October", "4", "2017", "72215"],
    ["racewire.com", "131.42", "October", "3", "2017", "23272"],
    ["google *epicactionllc", "19.48", "October", "3", "2017", "01105"],
    ["altus dental insur", "55.42", "October", "1", "2017", "73129"],
    ["cartoon carwash", "70.01", "October", "1", "2017", "80150"],
    ["usa*premier services vend", "200.19", "October", "1", "2017", "22908"]
];

const tdArrayPurchaseAmountKathy = [
    ["abc*retro fitness", "207.72", "October", "10", "2017", "74108"],
    ["usa*premier services vend", "396.55", "October", "8", "2017", "97240"],
    ["blue apron order", "81.72", "October", "8", "2017", "89510"],
    ["fanball", "55.87", "October", "6", "2017", "98447"],
    ["tractor supply co #704", "233.97", "October", "4", "2017", "72215"],
    ["cartoon carwash", "140.01", "October", "22", "2017", "80150"],
    ["usa*premier services vend", "200.91", "October", "21", "2017", "22908"],
    ["abc*retro fitness", "20.0", "October", "18", "2017", "28235"]
];

const accountPaymentsHeaders = ["Month","Starting Balance","Balance Paid", "Remaining Balance"];
const accountPaymentsData = [
    [ "January" , "$3,133.31" , "$3,133.31" , "$0.00" ] ,
    [ "February" , "$8,811.67" , "$8,811.67" , "$0.00" ] ,
    [ "March" , "$4,258.23" , "$4,258.23" , "$0.00" ] ,
    [ "April" , "$6,158.28" , "$6,158.28" , "$0.00" ] ,
    [ "May" , "$3,093.41" , "$3,093.41", , "$0.00" ] ,
    [ "June" , "$0,182.37" , "$0,182.37", "$0.00" ],
    [ "July" , "$4,880.81" , "$4,880.81", "$0.00" ],
    [ "August" , "$3,500.87" , "$3,500.87", "$0.00" ],
    [ "September" , "$6,990.64" , "$6,990.64", "$0.00" ],
    [ "October" , "$4,452.05" , "$4,452.05", "$0.00" ],
    [ "November" , "$7,179.74" , "$7,179.74", "$0.00" ],
    [ "December" , "$6,658.98" , "$6,658.98", "$0.00" ]
];

const accountRewardsHeaders = ["Month","Rewards Earned","Rewards Used"];
const accountRewardsData = [
    [ "January" , "$270.41" , "$10.80" ] ,
    [ "February" , "$196.13" , "$12.80" ] ,
    [ "March" , "$143.12" , "$16.09" ] ,
    [ "April" , "$211.24" , "$14.39" ] ,
    [ "May" , "$249.88" , "$22.91" ] ,
    [ "June" , "$219.41" , "$16.03" ],
    [ "July" , "$244.84" , "$13.65" ],
    [ "August" , "$177.21" , "$11.77" ],
    [ "September" , "$194.68" , "$26.98" ],
    [ "October" , "$167.82" , "$22.09" ],
    [ "November" , "$187.95" , "$22.03" ],
    [ "December" , "$149.73" , "$18.57" ]
];


//
// //
// // // For icons
// //
//
const iconsArray = [
    "pe-7s-album",
    "pe-7s-arc",
    "pe-7s-back-2",
    "pe-7s-bandaid",
    "pe-7s-car",
    "pe-7s-diamond",
    "pe-7s-door-lock",
    "pe-7s-eyedropper",
    "pe-7s-female",
    "pe-7s-gym",
    "pe-7s-hammer",
    "pe-7s-headphones",
    "pe-7s-helm",
    "pe-7s-hourglass",
    "pe-7s-leaf",
    "pe-7s-magic-wand",
    "pe-7s-male",
    "pe-7s-map-2",
    "pe-7s-next-2",
    "pe-7s-paint-bucket",
    "pe-7s-pendrive",
    "pe-7s-photo",
    "pe-7s-piggy",
    "pe-7s-plugin",
    "pe-7s-refresh-2",
    "pe-7s-rocket",
    "pe-7s-settings",
    "pe-7s-shield",
    "pe-7s-smile",
    "pe-7s-usb",
    "pe-7s-vector",
    "pe-7s-wine",
    "pe-7s-cloud-upload",
    "pe-7s-cash",
    "pe-7s-close",
    "pe-7s-bluetooth",
    "pe-7s-cloud-download",
    "pe-7s-way",
    "pe-7s-close-circle",
    "pe-7s-id",
    "pe-7s-angle-up",
    "pe-7s-wristwatch",
    "pe-7s-angle-up-circle",
    "pe-7s-world",
    "pe-7s-angle-right",
    "pe-7s-volume",
    "pe-7s-angle-right-circle",
    "pe-7s-users",
    "pe-7s-angle-left",
    "pe-7s-user-female",
    "pe-7s-angle-left-circle",
    "pe-7s-up-arrow",
    "pe-7s-angle-down",
    "pe-7s-switch",
    "pe-7s-angle-down-circle",
    "pe-7s-scissors",
    "pe-7s-wallet",
    "pe-7s-safe",
    "pe-7s-volume2",
    "pe-7s-volume1",
    "pe-7s-voicemail",
    "pe-7s-video",
    "pe-7s-user",
    "pe-7s-upload",
    "pe-7s-unlock",
    "pe-7s-umbrella",
    "pe-7s-trash",
    "pe-7s-tools",
    "pe-7s-timer",
    "pe-7s-ticket",
    "pe-7s-target",
    "pe-7s-sun",
    "pe-7s-study",
    "pe-7s-stopwatch",
    "pe-7s-star",
    "pe-7s-speaker",
    "pe-7s-signal",
    "pe-7s-shuffle",
    "pe-7s-shopbag",
    "pe-7s-share",
    "pe-7s-server",
    "pe-7s-search",
    "pe-7s-film",
    "pe-7s-science",
    "pe-7s-disk",
    "pe-7s-ribbon",
    "pe-7s-repeat",
    "pe-7s-refresh",
    "pe-7s-add-user",
    "pe-7s-refresh-cloud",
    "pe-7s-paperclip",
    "pe-7s-radio",
    "pe-7s-note2",
    "pe-7s-print",
    "pe-7s-network",
    "pe-7s-prev",
    "pe-7s-mute",
    "pe-7s-power",
    "pe-7s-medal",
    "pe-7s-portfolio",
    "pe-7s-like2",
    "pe-7s-plus",
    "pe-7s-left-arrow",
    "pe-7s-play",
    "pe-7s-key",
    "pe-7s-plane",
    "pe-7s-joy",
    "pe-7s-photo-gallery",
    "pe-7s-pin",
    "pe-7s-phone",
    "pe-7s-plug",
    "pe-7s-pen",
    "pe-7s-right-arrow",
    "pe-7s-paper-plane",
    "pe-7s-delete-user",
    "pe-7s-paint",
    "pe-7s-bottom-arrow",
    "pe-7s-notebook",
    "pe-7s-note",
    "pe-7s-next",
    "pe-7s-news-paper",
    "pe-7s-musiclist",
    "pe-7s-music",
    "pe-7s-mouse",
    "pe-7s-more",
    "pe-7s-moon",
    "pe-7s-monitor",
    "pe-7s-micro",
    "pe-7s-menu",
    "pe-7s-map",
    "pe-7s-map-marker",
    "pe-7s-mail",
    "pe-7s-mail-open",
    "pe-7s-mail-open-file",
    "pe-7s-magnet",
    "pe-7s-loop",
    "pe-7s-look",
    "pe-7s-lock",
    "pe-7s-lintern",
    "pe-7s-link",
    "pe-7s-like",
    "pe-7s-light",
    "pe-7s-less",
    "pe-7s-keypad",
    "pe-7s-junk",
    "pe-7s-info",
    "pe-7s-home",
    "pe-7s-help2",
    "pe-7s-help1",
    "pe-7s-graph3",
    "pe-7s-graph2",
    "pe-7s-graph1",
    "pe-7s-graph",
    "pe-7s-global",
    "pe-7s-gleam",
    "pe-7s-glasses",
    "pe-7s-gift",
    "pe-7s-folder",
    "pe-7s-flag",
    "pe-7s-filter",
    "pe-7s-file",
    "pe-7s-expand1",
    "pe-7s-exapnd2",
    "pe-7s-edit",
    "pe-7s-drop",
    "pe-7s-drawer",
    "pe-7s-download",
    "pe-7s-display2",
    "pe-7s-display1",
    "pe-7s-diskette",
    "pe-7s-date",
    "pe-7s-cup",
    "pe-7s-culture",
    "pe-7s-crop",
    "pe-7s-credit",
    "pe-7s-copy-file",
    "pe-7s-config",
    "pe-7s-compass",
    "pe-7s-comment",
    "pe-7s-coffee",
    "pe-7s-cloud",
    "pe-7s-clock",
    "pe-7s-check",
    "pe-7s-chat",
    "pe-7s-cart",
    "pe-7s-camera",
    "pe-7s-call",
    "pe-7s-calculator",
    "pe-7s-browser",
    "pe-7s-box2",
    "pe-7s-box1",
    "pe-7s-bookmarks",
    "pe-7s-bicycle",
    "pe-7s-bell",
    "pe-7s-battery",
    "pe-7s-ball",
    "pe-7s-back",
    "pe-7s-attention",
    "pe-7s-anchor",
    "pe-7s-albums",
    "pe-7s-alarm",
    "pe-7s-airplay"
];

//
// //
// // // // For dashboard's charts
// //
//
// Data for Pie Chart
var dataPie = {
    labels: ['20%','35%','45%'],
    series: [20, 35, 25]
};
var legendPie = {
    names: ["Allen","Janet","You"],
    types: ["info","danger","warning"]
};

// Data for Line Chart
var dataSales = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  series: [
    [4445.65, 5334.93, 7022.65, 4864.08, 4073.79, 5113.68, 4536.49, 5911.99, 6055.13, 6288.72, 6616.81, 6529.42],
    [4179.65, 6084.13, 4464.37, 4240.5, 5717.94, 4257.16, 4730.54, 3410.96, 4592.77, 3794.48, 4882.56, 4251.09],
    [5676.44, 4935.67, 4368.71, 4567.54, 3018.06, 4672.04, 3907.91, 3357.41, 4929.95, 3564.61, 3766.99, 3868.41]
  ]
};
var optionsSales = {
  low: 2000,
  high: 8000,
  showArea: false,
  height: "245px",
  axisX: {
    showGrid: false,
  },
  lineSmooth: true,
  showLine: true,
  showPoint: true,
  fullWidth: true,
  chartPadding: {
    right: 50
  }
};
var responsiveSales = [
  ['screen and (max-width: 640px)', {
    axisX: {
      labelInterpolationFnc: function (value) {
        return value[0];
      }
    }
  }]
];
var legendSales = {
    names: ["Original","(SELECTED) Savings+","Savings++"],
    types: ["info","danger","warning"]
};

// Data for Bar Chart
var dataBar = {
  labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  series: [
    [1542, 1443, 1320, 1780, 1553, 1453, 1326, 1434, 1568, 1610, 1756, 1895],
    [1412, 1243, 1280, 1580, 1453, 1353, 1300, 1364, 1368, 1410, 1636, 1695]
  ]
};
var optionsBar = {
    seriesBarDistance: 10,
    axisX: {
        showGrid: false
    },
    height: "245px"
};
var responsiveBar = [
  ['screen and (max-width: 640px)', {
    seriesBarDistance: 5,
    axisX: {
      labelInterpolationFnc: function (value) {
        return value[0];
      }
    }
  }]
];
var legendBar = {
    names: ["Allen","Janet"],
    types: ["info","danger"]
};

module.exports = {
    style, // For notifications (App container and Notifications view)
    thArrayPurchaseAmountPersonal, tdArrayPurchaseAmountPersonal, tdArrayPurchaseAmountAllen, tdArrayPurchaseAmountJohn,accountPaymentsHeaders, accountPaymentsData,
    accountRewardsHeaders, accountRewardsData, // For tables (TableList view)
    iconsArray, // For icons (Icons view)
    dataPie, legendPie, dataSales, optionsSales, responsiveSales, legendSales, dataBar, optionsBar, responsiveBar, legendBar // For charts (Dashboard view)
};
