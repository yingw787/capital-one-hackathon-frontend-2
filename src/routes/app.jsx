import Dashboard from 'views/Dashboard/Dashboard';
import UserProfile from 'views/UserProfile/UserProfile';
import Transactions from 'views/Transactions/Transactions';
import AccountDetails from 'views/AccountDetails/AccountDetails';

const appRoutes = [
    { path: "/overview", name: "Overview", icon: "pe-7s-graph", component: Dashboard },
    { path: "/profile", name: "Your Profile", icon: "pe-7s-user", component: UserProfile },
    { path: "/transactions", name: "Your Transactions", icon: "pe-7s-piggy", component: Transactions },
    { path: "/details", name: "Account Details", icon: "pe-7s-star", component: AccountDetails },
    { redirect: true, path:"/", to:"/overview", name: "Overview" }
];

export default appRoutes;
